#!/usr/bin/bash

# make sure we have python-linux-procfs installed
if rpm -q --quiet python3-linux-procfs; then
    :
else
    sudo dnf install -y python3-linux-procfs
    if [[ $? != 0 ]]; then
       echo "install of python3-linux-procfs failed!"
       exit 1
    fi
fi

# see if pflags is installed and executable
pflags --help || exit 2
pflags || exit 3

exit 0
